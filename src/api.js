import axios from "axios";
import xml2js from "xml2js";

export const getCurrency = async () => {
  return await axios
    .get(
      "https://cors-anywhere.herokuapp.com/http://www.cbr.ru/scripts/XML_daily.asp"
    )
    .then((response) => {
      let currencyData = [];
      xml2js.parseString(
        response.data,
        (err, result) => (currencyData = result.ValCurs.Valute)
      );
      return currencyData;
    });
};
