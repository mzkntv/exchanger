import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import lodash from "lodash";

import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;
Vue.use(lodash);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
